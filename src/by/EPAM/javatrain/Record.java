package by.EPAM.javatrain;

import java.util.*;
import java.text.*;

public class Record  implements Comparable<Record> {
  String surname;
  String name;
  String secName;
  String phone;
  String addr;
  Date dBirth;
 
  /***
   * базовый конструктор ни о чем
   */
  public Record () {
	  dBirth=new Date();
  }

  /***
   * конструктор с инициализацией всех полей
   */
  public Record (String surname, String name, String secName, String phone, String addr, Date dBirth ) {
	  this.dBirth=dBirth;
	  this.surname=surname;
	  this.name =name ;
	  this.secName = secName;
	  this.phone = phone;
	  this.addr = addr;
  }
  
	/***
	 * лучше использовать toString
	 */
  public void show () {
	  DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	  System.out.format("%18s%18s%18s%18s%18s%18s\n",surname,name,secName,phone, addr,((dBirth==null)?"null":df.format(dBirth)));
  }
/***
 * для сортировки по умолчанию. 
 * По убыванию фамилии
 */
  @Override
  public int compareTo(Record r) {
	  return  (-this.surname.compareTo(r.surname));
  }  
  /***
   * представление объекта строкой.
   * Переопределим базовый метод.
   */
  @Override
  public String toString() {
	  DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	  return  String.format("%18s%18s%18s%18s%18s%18s\n",surname,name,secName,phone, addr,((dBirth==null)?"null":df.format(dBirth)));
  }
  
  /***
   * для быстрого поиска по ключевым полям
   */
  @Override
  public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		return result;
  }
  
  /***
   * для быстрого поиска по ключевым полям
   * для операция сравнения объектов друг с другом
   */
  @Override
  public boolean equals(Object obj) {
	if (this == obj) {
		return true;
	}
	if (obj == null) {
		return false;
	}
	if (getClass() != obj.getClass()) {
		return false;
	}
	Record other = (Record) obj;
	if (phone == null) {
		if (other.phone != null) {
			return false;
		}
	} else if (!phone.equals(other.phone)) {
		return false;
	}
	if (surname == null) {
		if (other.surname != null) {
			return false;
		}
	} else if (!surname.equals(other.surname)) {
		return false;
	}
	return true;
  }

public String getAddr() {
	return addr;
  }
  public Date getdBirth() {
	return dBirth;
  }
  public String getName() {
	return name;
  }
  public String getPhone() {
	return phone;
  }
  public String getSecName() {
	return secName;
  }
  public String getSurname() {
	return surname;
  }
  
  public void setAddr(String addr) {
	this.addr = addr;
  }
  public void setdBirth(Date dBirth) {
	this.dBirth = dBirth;
  }
  public void setName(String name) {
	this.name = name;
  }
  public void setPhone(String phone) {
	this.phone = phone;
  }
  public void setSecName(String secName) {
	this.secName = secName;
  }
  public void setSurname(String surname) {
	this.surname = surname;
  }
  
}
